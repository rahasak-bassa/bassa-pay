# bassa-pay

bassa payment gateway api

# run bassa-pay service

## run with docker

```bash
docker run -d --name=bassapay -p 8761:8761 erangaeb/bassapay:0.1
```

## run with docker-compose

```bash
# docker compose entry
bassapay:
    image: erangaeb/bassapay:0.1
    container_name: bassapay
    ports:
        - 8761:8761
        
# run
docker-compose up -d bassapay
```

# test bassa-pay apis

## card payment

```bash
# request
curl -XPOST "http://localhost:8761/api/payments" \
--header "Content-Type: application/json" \
--data '
{
  "uid": "323233",
  "customerName": "eranga",
  "customerEmail": "e@trace.com",
  "cardNumber": "4242424242424242",
  "cardCvv": "883",
  "cardExpireDate": "0822",
  "cardType": "credit",
  "amount": "2312"
}
'

# success response
HTTP 200
{"code":200,"msg":"payment completed, transId - 60150835061"}

# fail response
HTTP 400
{"code":400,"msg":"Invalid credit card number"}
```

## bank payment/ach payment

```bash
# request
curl -XPOST "http://localhost:8761/api/payments" \
--header "Content-Type: application/json" \
--data '
{
  "uid": "323233",
  "customerName": "John Do",
  "customerEmail": "j@trace.com",
  "routingNumber": "021000021",
  "accountNumber": "111111111",
  "amount": "80"
}
'

# success response
HTTP 200
{"code":200,"msg":"payment completed, transId - 60150835021"}

# fail response
HTTP 400
{"code":400,"msg":"Invalid credit card number"}
```