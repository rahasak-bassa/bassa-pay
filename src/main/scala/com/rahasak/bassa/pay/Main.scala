package com.rahasak.bassa.pay

import akka.actor.ActorSystem
import com.rahasak.bassa.pay.actor.HttpActor
import com.rahasak.bassa.pay.actor.HttpActor.Serve
import com.rahasak.bassa.pay.config.AppConf
import com.rahasak.bassa.pay.util.LoggerFactory

object Main extends App with AppConf {

  // setup logging
  LoggerFactory.init()

  // actor system mystiko
  implicit val system = ActorSystem.create("bassa")

  system.actorOf(HttpActor.props(), name = "HttpActor") ! Serve

}

