package com.rahasak.bassa.pay.protocol

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.rahasak.bassa.pay.actor.PaymentActor.{CardPayment, DirectPayment}
import spray.json.DefaultJsonProtocol

object PaymentMessageProtocol extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val cardPaymentFormat = jsonFormat8(CardPayment)
  implicit val directPaymentFormat = jsonFormat6(DirectPayment)

}

