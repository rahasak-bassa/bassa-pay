package com.rahasak.bassa.pay.cassandra

import java.util.Date

import com.dataoperandz.cassper.Cassper
import com.rahasak.bassa.pay.util.AppLogger

import scala.collection.JavaConverters._


case class Account(did: String, owner: String,
                   password: String, typ: String, roles: List[String], pubKey: String,
                   deviceToken: String, deviceType: String, deviceImei: String,
                   answer1: String, answer2: String, answer3: String,
                   nic: String,
                   name: String,
                   dob: String,
                   phone: String,
                   email: String,
                   taxNo: String,
                   address: String,
                   blobId: String,
                   employeeName: String, occupation: String, employeeAddress: String,
                   salt: String, attempts: Int, activated: Boolean, verified: Boolean, disabled: Boolean, timestamp: Date = new Date)

case class Trans(id: String, execer: String, actor: String, messageTyp: String, message: String, digsig: String, timestamp: Date = new Date)

case class Trace(id: String, accountDid: String, accountNic: String, accountName: String, accountPhone: String,
                 accountOwnerDid: String, accountOwnerName: String,
                 accountTracerDid: String, accountTracerName: String, accountTracerOwner: String, accountTracerAddress: String,
                 salt: String, signature: String, verified: Boolean, lat: Double, lon: Double, timestamp: Date = new Date())

case class Consent(id: String, accountDid: String, accountNic: String, accountName: String, accountPhone: String,
                   accountOwnerDid: String, accountOwnerName: String,
                   accountConsenterDid: String, accountConsenterName: String, accountConsenterAddress: String,
                   status: String,
                   timestamp: Date = new Date())

case class Blob(id: String, blob: String, timestamp: Date = new Date())

object CassandraStore extends CassandraCluster with AppLogger {

  // transaction
  lazy val dsps = session.prepare("SELECT id FROM mystiko.trans WHERE execer = ? AND actor = ? AND id = ? LIMIT 1")
  lazy val ctps = session.prepare("INSERT INTO mystiko.trans(id, execer, actor, message_typ, message, digsig, timestamp) VALUES(?, ?, ?, ?, ?, ?, ?)")

  // account
  lazy val caps = session.prepare(
    "INSERT INTO mystiko.accounts(did, owner, password, typ, roles, pub_key, device_token, device_type, device_imei," +
      "answer1, answer2, answer3, nic, name, dob, phone, email, tax_no, address, blob_id, employee_name, occupation, employee_address, " +
      "salt, attempts, activated, verified, disabled, timestamp) " +
      "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val aaps = session.prepare("UPDATE mystiko.accounts SET activated = ? WHERE did = ? AND owner = ?")
  lazy val raps = session.prepare("UPDATE mystiko.accounts SET password = ?, device_type = ?, device_token =?, activated=? WHERE did = ? AND owner = ?")
  lazy val vaps = session.prepare("UPDATE mystiko.accounts SET verified = ? WHERE did = ? AND owner = ?")
  lazy val daps = session.prepare("UPDATE mystiko.accounts SET disabled = ? WHERE did = ? AND owner = ?")
  lazy val uaps = session.prepare("UPDATE mystiko.accounts SET attempts = ? WHERE did = ? AND owner = ?")
  lazy val uadps = session.prepare("UPDATE mystiko.accounts SET device_type = ?, device_token = ? WHERE did = ? AND owner = ?")
  lazy val uansps = session.prepare("UPDATE mystiko.accounts SET answer1 = ?, answer2 = ?, answer3 = ? WHERE did = ? AND owner = ?")
  lazy val gaps = session.prepare("SELECT * FROM mystiko.accounts where did = ? AND owner = ? LIMIT 1")
  lazy val uvcps = session.prepare("UPDATE mystiko.accounts SET salt = ? WHERE did = ? AND owner = ?")
  lazy val upassps = session.prepare("UPDATE mystiko.accounts SET password = ? WHERE did = ? AND owner = ?")

  // trace
  lazy val ctrps = session.prepare("INSERT INTO mystiko.traces(id, account_did, account_nic, account_name, account_phone, " +
    "account_owner_did, account_owner_name, account_tracer_did, account_tracer_name, account_tracer_owner, account_tracer_address, salt, signature, verified, location, timestamp) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val gtrps = session.prepare("SELECT * FROM mystiko.traces where id = ? AND account_did = ? LIMIT 1")

  // consents
  lazy val csps = session.prepare("INSERT INTO mystiko.consents(id, account_did, account_nic, account_name, account_phone, " +
    "account_owner_did, account_owner_name, " +
    "account_consenter_did, account_consenter_name, account_consenter_address, status, timestamp) " +
    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
  lazy val ucps = session.prepare("UPDATE mystiko.consents SET status = ?, timestamp = ? WHERE id = ? AND account_did = ?")
  lazy val gshrps = session.prepare("SELECT * FROM mystiko.consents where id = ? AND account_did = ? LIMIT 1")
  lazy val gshrsps = session.prepare("SELECT * FROM mystiko.consents where account_did = ? ALLOW FILTERING")

  // blob
  lazy val cbps = session.prepare("INSERT INTO mystiko.blobs(id, blob, timestamp) VALUES(?, ?, ?)")
  lazy val ubps = session.prepare("UPDATE mystiko.blobs SET blob = ? WHERE id = ?")
  lazy val gbps = session.prepare("SELECT * FROM mystiko.blobs where id = ? LIMIT 1")

  def init(): Unit = {
    // create keyspace
    session.execute(cassandraKeyspaceSchema)

    // migration via cassper
    // create udts, tables
    val builder = new Cassper().build("mystiko", session)
    builder.migrate("mystiko")

    // create admin user
    val admin = Account(
      "admin", "admin", "admin", "admin", List(), "pubkey", "", "", "", "", "", "", "", "eranga", "", "", "admin@connect.com",
      "", "", "", "", "", "", "", 0, true, true, false)
    CassandraStore.createAccount(admin)
  }

  def isDoubleSpend(execer: String, actor: String, id: String): Boolean = {
    // check weather given trans with id exists
    val row = session.execute(dsps.bind(execer, actor, id)).one()
    row != null
  }

  def createTrans(trans: Trans) = {
    session.execute(ctps.bind(trans.id, trans.execer, trans.actor, trans.messageTyp, trans.message,
      trans.digsig, trans.timestamp))
  }

  def createAccount(account: Account) = {
    session.execute(caps.bind(
      account.did,
      account.owner,
      account.password,
      account.typ,
      account.roles.toSet.asJava: java.util.Set[String],
      account.pubKey,
      account.deviceToken,
      account.deviceType,
      account.deviceImei,
      account.answer1,
      account.answer2,
      account.answer3,
      account.nic,
      account.name,
      account.dob,
      account.phone,
      account.email,
      account.taxNo,
      account.address,
      account.blobId,
      account.employeeName,
      account.occupation,
      account.employeeAddress,
      account.salt,
      account.attempts: java.lang.Integer,
      account.activated: java.lang.Boolean,
      account.verified: java.lang.Boolean,
      account.disabled: java.lang.Boolean,
      account.timestamp))
  }

  def registerAccount(account: Account) = {
    session.execute(raps.bind(account.password, account.deviceType, account.deviceToken, account.activated: java.lang.Boolean, account.did, account.owner))
  }

  def getAccount(id: String, owner: String): Option[Account] = {
    val row = session.execute(gaps.bind(id, owner)).one()
    if (row != null) Option(Account(
      row.getString("did"),
      row.getString("owner"),
      row.getString("password"),
      row.getString("typ"),
      row.getSet("roles", classOf[String]).asScala.toList,
      row.getString("pub_key"),
      row.getString("device_token"),
      row.getString("device_type"),
      row.getString("device_imei"),
      row.getString("answer1"),
      row.getString("answer2"),
      row.getString("answer3"),
      row.getString("nic"),
      row.getString("name"),
      row.getString("dob"),
      row.getString("phone"),
      row.getString("email"),
      row.getString("tax_no"),
      row.getString("address"),
      row.getString("blob_id"),
      row.getString("employee_name"),
      row.getString("occupation"),
      row.getString("employee_address"),
      row.getString("salt"),
      row.getInt("attempts"),
      row.getBool("activated"),
      row.getBool("verified"),
      row.getBool("disabled"),
      row.getTimestamp("timestamp")))
    else None
  }

  def activateAccount(did: String, owner: String, activated: Boolean) = {
    // update owner
    session.execute(aaps.bind(activated: java.lang.Boolean, did, owner))
  }

  def verifyAccount(did: String, owner: String, verified: Boolean) = {
    // update owner
    session.execute(vaps.bind(verified: java.lang.Boolean, did, owner))
  }

  def disableAccount(did: String, owner: String, disabled: Boolean) = {
    // update owner
    session.execute(daps.bind(disabled: java.lang.Boolean, did, owner))
  }

  def updatePassword(did: String, owner: String, password: String) = {
    session.execute(upassps.bind(password, did, owner))
  }

  def updateSalt(did: String, owner: String, salt: String) = {
    session.execute(uvcps.bind(salt, did, owner))
  }

  def updateAttempts(did: String, owner: String, attempts: Int) = {
    // update owner
    session.execute(uaps.bind(attempts: java.lang.Integer, did, owner))
  }

  def updateDevice(did: String, owner: String, deviceType: String, deviceToken: String) = {
    session.execute(uadps.bind(deviceType, deviceToken, did, owner))
  }

  def updateAnswers(did: String, owner: String, answer1: String, answer2: String, answer3: String) = {
    session.execute(uansps.bind(answer1, answer2, answer3, did, owner))
  }

  def createTrace(trace: Trace) = {
    val geoPointType = cluster.getMetadata.getKeyspace(cassandraKeyspace).getUserType("geo_point")
    val uv = geoPointType.newValue()
    uv.setDouble("lat", trace.lat)
    uv.setDouble("lon", trace.lon)

    session.execute(ctrps.bind(trace.id, trace.accountDid,
      trace.accountNic, trace.accountName, trace.accountPhone,
      trace.accountOwnerDid, trace.accountName,
      trace.accountTracerDid,
      trace.accountTracerName,
      trace.accountTracerOwner,
      trace.accountTracerAddress,
      trace.salt, trace.signature, trace.verified: java.lang.Boolean, uv, trace.timestamp))
  }

  def getTrace(id: String, accountDid: String): Option[Trace] = {
    // watch object
    val row = session.execute(gtrps.bind(id, accountDid)).one()
    if (row != null) {
      val uv = row.getUDTValue("location")
      val lat = uv.getDouble("lat")
      val lon = uv.getDouble("lon")
      Option(Trace(row.getString("id"), row.getString("account_did"),
        row.getString("account_nic"), row.getString("account_name"), row.getString("account_phone"),
        row.getString("account_owner_did"), row.getString("account_owner_name"),
        row.getString("account_tracer_did"),
        row.getString("account_tracer_name"),
        row.getString("account_tracer_owner"),
        row.getString("account_tracer_address"),
        row.getString("salt"), row.getString("signature"), row.getBool("verified"), lat, lon,
        row.getTimestamp("timestamp")))
    }
    else None
  }

  def createConsents(consent: Consent) = {
    session.execute(csps.bind(consent.id, consent.accountDid,
      consent.accountNic, consent.accountName, consent.accountPhone,
      consent.accountOwnerDid,
      consent.accountOwnerName,
      consent.accountConsenterDid,
      consent.accountConsenterName,
      consent.accountConsenterAddress,
      consent.status,
      consent.timestamp))
  }

  def updateConsent(id: String, accountDid: String, status: String, timestamp: Date = new Date()) = {
    // update status
    session.execute(ucps.bind(status, timestamp, id, accountDid))
  }

  def getConsents(accountDid: String): List[Consent] = {
    val shares = scala.collection.mutable.MutableList[Consent]()
    val resultSet = session.execute(gshrsps.bind(accountDid)).iterator()
    while (resultSet.hasNext) {
      val row = resultSet.next()
      if (row != null) {
        val share = Consent(row.getString("id"),
          row.getString("account_did"),
          row.getString("account_nic"), row.getString("account_name"), row.getString("account_phone"),
          row.getString("account_owner_did"), row.getString("account_owner_name"),
          row.getString("account_consenter_did"),
          row.getString("account_consenter_name"),
          row.getString("account_consenter_address"),
          row.getString("status"),
          row.getTimestamp("timestamp"))
        shares += share
      }
    }

    shares.toList
  }

  def getConsent(id: String, accountDid: String): Option[Consent] = {
    val row = session.execute(gshrps.bind(id, accountDid)).one()
    if (row != null) {
      Option(Consent(row.getString("id"), row.getString("account_did"),
        row.getString("account_nic"), row.getString("account_name"), row.getString("account_phone"),
        row.getString("account_owner_did"), row.getString("account_owner_name"),
        row.getString("account_consenter_did"),
        row.getString("account_consenter_name"),
        row.getString("account_consenter_address"),
        row.getString("status"),
        row.getTimestamp("timestamp")))
    }
    else None
  }

  def createBlob(blob: Blob) = {
    session.execute(cbps.bind(blob.id, blob.blob, blob.timestamp))
  }

  def updateBlob(id: String, blob: String) = {
    session.execute(ubps.bind(blob, id))
  }

  def getBlob(id: String): Option[Blob] = {
    val row = session.execute(gbps.bind(id)).one()
    if (row != null) Option(Blob(row.getString("id"), row.getString("blob"),
      row.getTimestamp("timestamp")))
    else None
  }

}

//object M extends App {
//  CassandraStore.init()
//  println(CassandraStore.isDoubleSpend("eranga", "AccountActor", "112"))
//
//  val acc1 = Account(
//    "111", "222", "w32323", "user", List(), "pubkey", "2323", "2323", "2323", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  val acc2 = Account(
//    "111", "222", "password", "user", List(), "pubkey", "device_token", "apple", "imei", "1", "2", "3", "8736", "eranga", "78", "077",
//    "gmail", "11ee", "matale", "111", "pagero", "engi", "sweden", "2323", 0, false, true, false)
//  CassandraStore.createAccount(acc1)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.registerAccount(acc2)
//  println(CassandraStore.getAccount("111", "222"))
//  CassandraStore.activateAccount("111", "222", true)
//  CassandraStore.verifyAccount("111", "222", true)
//  //    CassandraStore.updateAttempts("775432015", 2)
//  //    CassandraStore.disableAccount("775432015", true)
//  CassandraStore.createTrans(Trans("112", "eranga", "AccountActor", "create", "{\"message\": \"java\"}", "1111"))
//
//  CassandraStore.createTrace(Trace("111", "111", "1212", "ops", "077", "222", "2323", "333", "hak", "matale", "232", "sig", false, 2.121, 4.2323))
//  println(CassandraStore.getTrace("111", "111"))
//
//  CassandraStore.createConsents(Consent("111", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  CassandraStore.createConsents(Consent("222", "111", "1212", "ops", "077", "222", "edd", "333", "hak", "matale"))
//  println(CassandraStore.getConsents("111"))
//}
