package com.rahasak.bassa.pay.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait PaymentConf {

  val paymentConf = ConfigFactory.load("payment.conf")

  lazy val gatewayApi = Try(paymentConf.getString("gateway-api")).getOrElse("https://test.authorize.net")
  lazy val merchantLoginId = Try(paymentConf.getString("merchant-login-id")).getOrElse("7EV4wry8NQ")
  lazy val merchantTransactionKey = Try(paymentConf.getString("merchant-transaction-key")).getOrElse("3tzpPfD56369dqYa")

}
