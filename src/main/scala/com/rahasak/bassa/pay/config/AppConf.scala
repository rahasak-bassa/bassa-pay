package com.rahasak.bassa.pay.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait AppConf {

  // config object
  val appConf = ConfigFactory.load()

  // service config
  lazy val serviceMode = Try(appConf.getString("service.mode")).getOrElse("DEV")
  lazy val serviceName = Try(appConf.getString("service.name")).getOrElse("bassa-pay")
  lazy val servicePort = Try(appConf.getInt("service.port")).getOrElse(8761)

  // keys config
  lazy val keysDir = Try(appConf.getString("keys.dir")).getOrElse(".keys")
  lazy val publicKeyLocation = Try(appConf.getString("keys.public-key-location")).getOrElse(".keys/id_rsa.pub")
  lazy val privateKeyLocation = Try(appConf.getString("keys.private-key-location")).getOrElse(".keys/id_rsa")

}
