package com.rahasak.bassa.pay.config

import com.typesafe.config.ConfigFactory

trait ElasticConf {
  val elasticConf = ConfigFactory.load("elastic.conf")

  lazy val elasticCluster = elasticConf.getString("elastic.cluster")

  lazy val transElasticIndex = elasticConf.getString("elastic.trans-index")
  lazy val transElasticDocType = elasticConf.getString("elastic.trans-doc-type")
  lazy val tracesElasticIndex = elasticConf.getString("elastic.traces-index")
  lazy val tracesElasticDocType = elasticConf.getString("elastic.traces-doc-type")
  lazy val consentsElasticIndex = elasticConf.getString("elastic.consents-index")
  lazy val consentsElasticDocType = elasticConf.getString("elastic.consents-doc-type")
  lazy val accountsElasticIndex = elasticConf.getString("elastic.accounts-index")
  lazy val accountsElasticDocType = elasticConf.getString("elastic.accounts-doc-type")

  lazy val elasticHosts = elasticConf.getString("elastic.hosts").split(",").toSeq
  lazy val elasticPort = elasticConf.getString("elastic.port").toInt
}