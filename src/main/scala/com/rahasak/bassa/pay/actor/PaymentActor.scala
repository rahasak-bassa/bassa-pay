package com.rahasak.bassa.pay.actor

import java.math.RoundingMode

import akka.actor.{Actor, Props}
import akka.stream.ActorMaterializer
import com.rahasak.bassa.pay.actor.PaymentActor.{CardPayment, DirectPayment}
import com.rahasak.bassa.pay.config.{AppConf, PaymentConf}
import com.rahasak.bassa.pay.protocol.StatusReply
import com.rahasak.bassa.pay.util.AppLogger
import net.authorize.Environment
import net.authorize.api.contract.v1._
import net.authorize.api.controller.CreateTransactionController
import net.authorize.api.controller.base.ApiOperationBase


object PaymentActor {

  case class CardPayment(uid: String, customerName: String, customerEmail: String, cardNumber: String, cardCvv: String, cardExpireDate: String, cardType: String, amount: String)

  case class DirectPayment(uid: String, customerName: String, customerEmail: String, routingNumber: String, accountNumber: String, amount: String)

  def props()(implicit materializer: ActorMaterializer) = Props(new PaymentActor())

}

class PaymentActor()(implicit materializer: ActorMaterializer) extends Actor with AppConf with PaymentConf with AppLogger {
  override def receive: Receive = {
    case payment: CardPayment =>
      logger.info(s"got card payment - $payment")

      // make card payment
      val statusReply = makeCardPayment(payment)
      sender ! statusReply

      context.stop(self)

    case payment: DirectPayment =>
      logger.info(s"got direct payment - $payment")

      sender ! makeDirectPayment(payment)

      context.stop(self)
  }

  def makeCardPayment(payment: CardPayment): StatusReply = {
    logger.info(s"make card payment $payment in $serviceMode mode")

    // payment environment based on service mode
    serviceMode match {
      case "PROD" =>
        ApiOperationBase.setEnvironment(Environment.PRODUCTION)
      case _ =>
        ApiOperationBase.setEnvironment(Environment.SANDBOX)
    }

    // setup merchant auth details
    val merchantAuthenticationType = new MerchantAuthenticationType()
    merchantAuthenticationType.setName(merchantLoginId)
    merchantAuthenticationType.setTransactionKey(merchantTransactionKey)
    ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType)

    // credit card payment data
    val paymentType = new PaymentType()
    val creditCard = new CreditCardType()
    creditCard.setCardNumber(payment.cardNumber)
    creditCard.setExpirationDate(payment.cardExpireDate)
    paymentType.setCreditCard(creditCard)

    // set customer email (optional)
    val customer = new CustomerDataType()
    customer.setEmail(payment.customerEmail)

    // create the payment transaction request
    val txnRequest = new TransactionRequestType()
    txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value())
    txnRequest.setPayment(paymentType)
    txnRequest.setCustomer(customer)
    val amount = new java.math.BigDecimal(payment.amount).setScale(2, RoundingMode.CEILING)
    txnRequest.setAmount(amount)

    // create api request
    val apiRequest = new CreateTransactionRequest()
    apiRequest.setTransactionRequest(txnRequest)
    val controller = new CreateTransactionController(apiRequest)
    controller.execute()

    // handle response
    val response = controller.getApiResponse
    if (response != null) {
      // check transaction response
      if (response.getMessages.getResultCode == MessageTypeEnum.OK) {
        val result = response.getTransactionResponse
        if (result.getMessages != null) {
          logger.info(s"payment successful, authCode - ${result.getAuthCode}, transId - ${result.getTransId}")
          StatusReply(200, s"payment completed, transId - ${result.getTransId}")
        } else {
          logger.error(s"payment failed, transId - ${result.getTransId}, responseCode - ${result.getResponseCode}, error - ${result.getErrors.getError.get(0).getErrorText}, authCode - ${result.getAuthCode}, transId - ${result.getTransId}")
          StatusReply(400, s"payment failed transId - ${result.getTransId}, error - ${result.getErrors.getError.get(0).getErrorText}")
        }
      } else {
        val err = response.getTransactionResponse.getErrors.getError.get(0).getErrorText
        logger.error(s"payment failed, transId - ${response.getTransactionResponse.getTransId}, resultCode - ${response.getMessages.getResultCode}, error - $err")
        StatusReply(400, s"payment failed transId - ${response.getTransactionResponse.getTransId}, error - $err")
      }
    } else {
      logger.error(s"payment failed, null response")

      StatusReply(400, s"payment failed")
    }
  }

  def makeDirectPayment(payment: DirectPayment): StatusReply = {
    logger.info(s"make card payment $payment in $serviceMode mode")

    // payment environment based on service mode
    serviceMode match {
      case "PROD" =>
        ApiOperationBase.setEnvironment(Environment.PRODUCTION)
      case _ =>
        ApiOperationBase.setEnvironment(Environment.SANDBOX)
    }

    // setup merchant auth details
    val merchantAuthenticationType = new MerchantAuthenticationType()
    merchantAuthenticationType.setName(merchantLoginId)
    merchantAuthenticationType.setTransactionKey(merchantTransactionKey)
    ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType)

    // direct bank payment/ach payment data
    val paymentType = new PaymentType()
    val bankAccountType = new BankAccountType()
    bankAccountType.setAccountType(BankAccountTypeEnum.CHECKING)
    bankAccountType.setRoutingNumber(payment.routingNumber)
    bankAccountType.setAccountNumber(payment.accountNumber)
    bankAccountType.setNameOnAccount(payment.customerName)
    paymentType.setBankAccount(bankAccountType)

    // set customer email (optional)
    val driversLicense= new DriversLicenseType()
    driversLicense.setNumber("1212")
    driversLicense.setState("va")
    driversLicense.setDateOfBirth("08/12/1987")
    val customer = new CustomerDataType()
    customer.setEmail(payment.customerEmail)
    customer.setDriversLicense(driversLicense)

    // create the payment transaction request
    val txnRequest = new TransactionRequestType()
    txnRequest.setTransactionType(TransactionTypeEnum.AUTH_CAPTURE_TRANSACTION.value())
    txnRequest.setPayment(paymentType)
    txnRequest.setCustomer(customer)
    val amount = new java.math.BigDecimal(payment.amount).setScale(2, RoundingMode.CEILING)
    txnRequest.setAmount(amount)

    // create api request
    val apiRequest = new CreateTransactionRequest()
    apiRequest.setTransactionRequest(txnRequest)
    val controller = new CreateTransactionController(apiRequest)
    controller.execute()

    // handle response
    val response = controller.getApiResponse
    if (response != null) {
      // check transaction response
      if (response.getMessages.getResultCode == MessageTypeEnum.OK) {
        val result = response.getTransactionResponse
        if (result.getMessages != null) {
          logger.info(s"payment successful, authCode - ${result.getAuthCode}, transId - ${result.getTransId}")
          StatusReply(200, s"payment completed, transId - ${result.getTransId}")
        } else {
          logger.error(s"payment failed, transId - ${result.getTransId}, responseCode - ${result.getResponseCode}, error - ${result.getErrors.getError.get(0).getErrorText}, authCode - ${result.getAuthCode}, transId - ${result.getTransId}")
          StatusReply(400, s"payment failed transId - ${result.getTransId}, error - ${result.getErrors.getError.get(0).getErrorText}")
        }
      } else {
        val err = response.getTransactionResponse.getErrors.getError.get(0).getErrorText
        logger.error(s"payment failed, transId - ${response.getTransactionResponse.getTransId}, resultCode - ${response.getMessages.getResultCode}, error - $err")
        StatusReply(400, s"payment failed transId - ${response.getTransactionResponse.getTransId}, error - $err")
      }
    } else {
      logger.error(s"payment failed, null response")
      StatusReply(400, s"payment failed")
    }
  }
}

