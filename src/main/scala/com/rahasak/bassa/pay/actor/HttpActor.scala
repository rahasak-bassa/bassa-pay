package com.rahasak.bassa.pay.actor

import akka.actor.{Actor, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Supervision}
import akka.util.Timeout
import com.rahasak.bassa.pay.actor.HttpActor.Serve
import com.rahasak.bassa.pay.actor.PaymentActor.{CardPayment, DirectPayment}
import com.rahasak.bassa.pay.config.AppConf
import com.rahasak.bassa.pay.protocol.StatusReply
import com.rahasak.bassa.pay.util.AppLogger

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object HttpActor {

  case class Serve()

  def props()(implicit system: ActorSystem) = Props(new HttpActor)

}

class HttpActor()(implicit system: ActorSystem) extends Actor with AppLogger with AppConf {

  override def receive: Receive = {
    case Serve =>
      // supervision
      // meterializer for streams
      val decider: Supervision.Decider = { e =>
        logError(e)
        Supervision.Resume
      }
      implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))
      implicit val ec = system.dispatcher

      logger.info(s"starting api on port $servicePort")

      Http().bindAndHandle(route, "0.0.0.0", servicePort)
  }

  def route()(implicit materializer: ActorMaterializer, ec: ExecutionContextExecutor) = {
    implicit val timeout = Timeout(60.seconds)

    import com.rahasak.bassa.pay.protocol.StatusReplyProtocol._

    pathPrefix("api") {
      path("payments") {
        post {
          import com.rahasak.bassa.pay.protocol.PaymentMessageProtocol._
          entity(as[CardPayment]) { payment =>
            val f = context.actorOf(PaymentActor.props()) ? payment
            onComplete(f) {
              case Success(status: StatusReply) =>
                logger.info(s"complete the payment $status")
                complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
              case Failure(e) =>
                logError(e)
                complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the payment"))
              case _ =>
                logger.info("fail payment")
                complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the payment"))
            }
          }
        } ~
          post {
            import com.rahasak.bassa.pay.protocol.PaymentMessageProtocol._
            entity(as[DirectPayment]) { payment =>
              val f = context.actorOf(PaymentActor.props()) ? payment
              onComplete(f) {
                case Success(status: StatusReply) =>
                  logger.info(s"complete the payment $status")
                  complete(if (status.code == 200) StatusCodes.OK -> status else StatusCodes.BadRequest -> status)
                case Failure(e) =>
                  logError(e)
                  complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the payment"))
                case _ =>
                  logger.info("fail payment")
                  complete(StatusCodes.BadRequest -> StatusReply(400, "Failed to complete the payment"))
              }
            }
          }
      }
    }
  }
}
